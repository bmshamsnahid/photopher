var photographerService = require('../../service/photographerService');
var databaseConfig = require('../../config/database');
var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');
var ObjectId = require('mongodb').ObjectID;
var url = databaseConfig.url;
var fs = require('fs');

var createPhotographer = (req, res) => {

    MongoClient.connect(url, (err, db) => {
        assert.equal(null, err);
        photographerService.createPhotographer(db, req.body, (docs) => {
            db.close();
            res.status(200).json(docs);
        });
    });  
};

var getAllPhotographer = (req, res) => {
    MongoClient.connect(url, (err, db) => {
        assert.equal(null, err);
        photographerService.findAllPhotographer(db, (docs) => {
            db.close();
            res.status(200).json(docs);
        });
    });
};

var getSinglePhotographer = (req, res) => {
    MongoClient.connect(url, (err, db) => {
        assert.equal(null, err);
        
        var ObjectID = require('mongodb').ObjectID;
        var id = new ObjectID(req.params.id);
        
        photographerService.findSinglePhotographer(db, id, (docs) => {
            db.close();
            res.status(200).json(docs);
        });
    });
};

var updatePhotographer = (req, res) => {

    MongoClient.connect(url, (err, db) => {
        assert.equal(null, err);
        
        var ObjectID = require('mongodb').ObjectID;
        var id = new ObjectID(req.params.id);

        photographerService.findSinglePhotographer(db, id, (docs) => {
            
            var oldPhotographer = docs;
            var newPhotographer = req.body;
            if(typeof req.file != 'undefined') {
                newPhotographer.photographerProfilePhotoLocation = '/public/PhotographerImage/profilePhoto-' + req.file.filename;
            }

            newValues = { $set: newPhotographer };

            photographerService.updatePhotographer(db, id, newValues, (docs) => {
                db.close();
                res.status(200).json(docs);
            });
        });
    });
};

var deletePhotographer = (req, res) => {
    MongoClient.connect(url, (err, db) => {
        assert.equal(null, err);
        
        var ObjectID = require('mongodb').ObjectID;
        var id = new ObjectID(req.params.id);

        var filePath = './public/PhotographerImage/profilePhoto-' + req.params.id;
        fs.access(filePath, error => {
            if (!error) {
                fs.unlink(filePath,function(error){
                    console.log(error);
                });
            } else {
                console.log(error);
            }
        });
        
        photographerService.deletePhotographer(db, id, (docs) => {
            db.close();
            res.status(200).json(docs);
        });
    });
};

module.exports = {
    createPhotographer,
    getAllPhotographer,
    getSinglePhotographer, 
    updatePhotographer,
    deletePhotographer
};