var assert = require('assert');
var feedbackService = require('../../service/feedbackService');
var databaseConfig = require('../../config/database');
var MongoClient = require('mongodb').MongoClient;
var ObjectID = require('mongodb').ObjectId;



var url = databaseConfig.url;

var addFeedback = (req,res)=>{

    MongoClient.connect(url,(err,db)=>{
        assert.equal(null,err);
        feedbackService.insertFeedback(db,req.body,(docs)=>{
            res.status(200).json(docs);
            db.close();
        })
    })

}

var allFeedback = (req,res)=>{
    MongoClient.connect(url,(err,db)=>{
        assert.equal(null,err);

        feedbackService.findFeedbacks(db,(docs)=>{
            res.status(200).json(docs);
            db.close();
        })
    })
}

var findSingleFeedback = function (req,res) {

    var id = req.params.id;

    var o_id = new ObjectID(id);

    MongoClient.connect(url,(err,db) =>{
        assert.equal(err,null);

        feedbackService.findFeedback(db,o_id,(docs)=>{
            db.close();
            res.status(200).json(docs);
        })
    })
}

var editFeedback = (req,res) => {
    var id = req.params.id;

    var o_id = new ObjectID(id);

    MongoClient.connect(url,(err,db) =>{
        assert.equal(null,err);
        feedbackService.updateFeedback(db,o_id,req.body, (docs)=>{
            db.close();
            res.status(200).json(docs);
        })
    })
}

var deletFeedback = (req,res)=>{
    var id = req.params.id;

    var o_id = new ObjectID(id);

    MongoClient.connect(url,(err,db)=>{
        assert.equal(err,null);

        feedbackService.removeFeedback(db,o_id,(docs)=>{
            res.status(200).json(docs);
            db.close();
        })
    })
}

module.exports = {
    addFeedback,
    findSingleFeedback,
    deletFeedback,
    allFeedback,
    editFeedback
}