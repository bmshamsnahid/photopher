var photoService = require('../../service/photoService');
var databaseConfig = require('../../config/database');
var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');
var ObjectId = require('mongodb').ObjectID;
var url = databaseConfig.url;
var fs = require('fs');


var addPhoto = (req, res) => {
    var photoInfo = req.body;
    if(typeof req.file != "undefined") { //checking if a photo is uploaded or not
        photoInfo.filePath = './public/uploads/' + req.file.filename; //setting path for new added photo
    }
    photoInfo.UserID = req.user._id;


    MongoClient.connect(url, (err, db) => {
        assert.equal(null, err);
        photoService.uploadPhoto(db, photoInfo, (docs) => {   //calling 'uploadPhoto' to add photo
            db.close();
            res.status(200).json(docs);
        });
    });
};

var getAllPhoto = (req, res) => {
    MongoClient.connect(url, (err, db) => {
        assert.equal(null, err);
        photoService.viewAllPhoto(db, (docs) => {  //calling 'viewAllPhoto' to view all photo
            db.close();
            res.status(200).json(docs);
        });
    });
};

var getAPhoto = (req, res) => {
    MongoClient.connect(url, (err, db) => {
        assert.equal(null, err);

        var ObjectID = require('mongodb').ObjectID;
        var id = new ObjectID(req.params.id);   //get id

        photoService.viewAPhoto(db, id, (docs) => {  //calling 'viewAPhoto' to view a photo by id
            db.close();
            res.status(200).json(docs);
        });
    });
};

var deleteAPhoto = (req, res) => {
    MongoClient.connect(url, (err, db) => {
        assert.equal(null, err);

        var ObjectID = require('mongodb').ObjectID;
        var id = new ObjectID(req.params.id);
        var collection = db.collection('photos');
        collection.findOne({'_id': id}, function(err, docs) {   //find the photo's info's by id in 'docs'
            assert.equal(err, null);

            var filePath = docs.filePath;   //get 'filePath' of the photo
            console.log("FilePath: " + docs.filePath);

            fs.access(filePath, error => {
                //remove the photo from disk(server) if there have no error, else print error
                if (!error) {
                    fs.unlink(filePath,function(error){
                        console.log('Error is : ' + JSON.stringify(error));
                    });
                } else {
                    console.log('Error Is : ' + JSON.stringify(error));
                }
            });
        });

        //calling 'deletePhoto' to delete photo from database
        photoService.deletePhoto(db, id, (docs) => {
            db.close();
            res.status(200).json(docs);
        });
    });
};

var updatePhoto = (req, res) => {

    MongoClient.connect(url, (err, db) => {
        assert.equal(null, err);

        var ObjectID = require('mongodb').ObjectID;
        var id = new ObjectID(req.params.id);

        photoService.viewAPhoto(db, id, (docs) => {   // get old photo by id in 'docs'

            var oldPhoto = docs;
            var newPhoto = req.body;
            console.log(JSON.stringify(req.body));
            if(typeof req.file != 'undefined') {    //checking if a new photo uploaded
                newPhoto.filePath = './public/uploads/' + req.file.filename;   //setting path for new added photo

                //since we are updating a photo, we need to delete the existing photo
                var filePath = oldPhoto.filePath;
                console.log("Old File's Path: " + oldPhoto.filePath);

                fs.access(filePath, error => {
                    //remove the photo from disk(server) if there have no error, else print error
                    if (!error) {
                        fs.unlink(filePath,function(error){
                            console.log('Error is : ' + JSON.stringify(error));
                        });
                    } else {
                        console.log('Error Is : ' + JSON.stringify(error));
                    }
                });
            }

            //update the database
            newValues = { $set: newPhoto };

            photoService.updatePhoto(db, id, newValues, (docs) => {
                db.close();
                res.status(200).json(docs);
            });
        });
    });
};

module.exports = {
    addPhoto,
    getAllPhoto,
    getAPhoto,
    deleteAPhoto,
    updatePhoto
};