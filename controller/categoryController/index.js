var assert = require('assert');
var categoryService = require('../../service/categoryService');
var databaseConfig = require('../../config/database');
var MongoClient = require('mongodb').MongoClient;
var ObjectID = require('mongodb').ObjectId;

var url = databaseConfig.url;

var createCategory = (req,res)=>{

    MongoClient.connect(url,(err,db)=>{
        assert.equal(null,err);
        categoryService.createCategory(db, req.body, (docs)=>{
            db.close();
            res.status(200).json(docs);
        })
    })

}

var findAllCategory = (req,res)=>{
    MongoClient.connect(url,(err,db)=>{
        assert.equal(null,err);
        categoryService.findAllCategory(db,(docs)=>{
            db.close();
            res.status(200).json(docs);
        })
    })
}

var findSingleCategory = (req,res) => {

    var id = new ObjectID(req.params.id);

    MongoClient.connect(url,(err,db) =>{
        assert.equal(err,null);
        categoryService.findSingleCategory(db, id, (docs)=> {
            db.close();
            res.status(200).json(docs);
        })
    })
}

var updateCategory = (req,res) => {
    var id = new ObjectID(req.params.id);

    MongoClient.connect(url,(err,db) =>{
        assert.equal(null,err);
        categoryService.updateCategory(db, id, req.body, (docs) => {
            db.close();
            res.status(200).json(docs);
        })
    })
}

var deleteCategory = (req,res)=>{
    var id = new ObjectID(req.params.id);

    MongoClient.connect(url, (err,db)=>{
        assert.equal(err,null);
        categoryService.deleteCategory(db, id,(docs)=>{
            res.status(200).json(docs);
            db.close();
        })
    })
}

module.exports = {
   createCategory,
   findAllCategory,
   findSingleCategory,
   updateCategory,
   deleteCategory
}