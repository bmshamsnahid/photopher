var assert = require('assert');
var messageService = require('../../service/messageService');
var databaseConfig = require('../../config/database');
var MongoClient = require('mongodb').MongoClient;
var ObjectID = require('mongodb').ObjectId;



var url = databaseConfig.url;

var addMessage = (req,res)=>{

    MongoClient.connect(url,(err,db)=>{
        assert.equal(null,err);
        messageService.insertMessage(db,req.body,(docs)=>{
            res.status(200).json(docs);
            db.close();
        })
    })

}

var allMessage = (req,res)=>{
    MongoClient.connect(url,(err,db)=>{
        assert.equal(null,err);

        messageService.findMessages(db,(docs)=>{
            res.status(200).json(docs);
            db.close();
        })
    })
}

var findSingleMessage = function (req,res) {

    var id = req.params.id;

    var o_id = new ObjectID(id);

    MongoClient.connect(url,(err,db) =>{
        assert.equal(err,null);

        messageService.findMessage(db,o_id,(docs)=>{
            db.close();
            res.status(200).json(docs);
        })
    })
}

var editMessage = (req,res) => {
    var id = req.params.id;

    var o_id = new ObjectID(id);

    MongoClient.connect(url,(err,db) =>{
        assert.equal(null,err);
        messageService.updateMessage(db,o_id,req.body, (docs)=>{
            db.close();
            res.status(200).json(docs);
        })
    })
}

var deletMessage = (req,res)=>{
    var id = req.params.id;

    var o_id = new ObjectID(id);

    MongoClient.connect(url,(err,db)=>{
        assert.equal(err,null);

        messageService.removeMessage(db,o_id,(docs)=>{
            res.status(200).json(docs);
            db.close();
        })
    })
}

module.exports = {
   addMessage,
    findSingleMessage,
    allMessage,
    editMessage,
    deletMessage
}